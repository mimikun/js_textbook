# js_textbook

## npm scripts
- build
    - `webpack`でビルド
- build:dev
    - `NODE_ENV=development webpack` でminifyありでビルド
- build:prod
    - `NODE_ENV=production webpack` minifyありでビルド
- build:tsc
    - `tsc src/* --outDir dist` でコンパイル
- start
    - `urlopen chrome dist/index.html` する
- lint
    - `tslint 'src/*.ts'` する
- test
    - jestでテスト
