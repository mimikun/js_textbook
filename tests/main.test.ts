import {hello} from "../src/main";

test("should disp hello", () => {
  expect(hello("hello")).toBe("hello-world");
});
